
serenity = window.serenity

class serenity.characters.CharacterManagementView extends Backbone.Marionette.CompositeView
  className: "serenity-character-management"
  template: _.template """
    <div class="serenity-character-management-buttons-bar"></div>
    <div class="serenity-character-management-characters">
      <div class="serenity-character-management-NPCs"></div>
      <div class="serenity-character-management-PCs"></div>
    </div>
  """

  regions:
    'buttons': 'serenity-character-management-buttons-bar'
    'NPCs': '.serenity-character-management-NPCs'
    'PCs': '.serenity-character-management-PCs'

  initialize: ( {@collection} ) ->
    @NPCs = @collection.subcollection
      filter: ( character ) -> character.isNPC()
    @NPCs.subcategory = {}
    _( @NPCs.pluck( 'NPCCategory' ) ).each ( category ) =>
      @NPCs.subcategory[category] = @NPCs.subcollection
        filter: ( character ) -> character.NPCCategory() is category
    @NPCView = new serenity.characters.NPCView( collection: @NPCs )

    @PCs = @collection.subcollection
      filter: ( character ) -> not character.isNPC()
    @PCView = new serenity.characters.PCView( collection: @PCs )

  onShow: ->
    @regions.NPCs.show( @NPCView )
    @regions.PCs.show( @PCView )
    @regions.buttons.show( new serenity.characters.CharacterManagementButtonsView() )


class serenity.characters.PCView extends Backbone.Marionette.CollectionView
  childView: serenity.characters.CharacterView
  className: 'serenity-character-pc-collection'

  initialize: ( { @collection } ) ->
    throw new Error('PCView can only be used with a collection of PCs') if _.any( @collection.pluck( 'isNPC' ) )

class serenity.characters.NPCCategoriesView extends Backbone.Marionette.CompositeView
  #display a list of categories to choose from

class serenity.characters.NPCView extends Backbone.Marionette.CollectionView
  childView: serenity.characters.CharacterView
  className: 'serenity-character-npc-collection'

  initialize: ( { @collection } ) ->
    throw new Error('NPCView can only be used with a collection of NPCs') if not _.all( @collection.pluck( 'isNPC' ), _.identity )

class serenity.characters.CharacterView extends Backbone.Marionette.ItemView
  className: 'serenity-character-view'
  template: _.template """
    <div class="tons of crap here"></div>
  """








class ascc.pTreeNodeView extends Backbone.Marionette.CompositeView
    tagName: "tr"
    className: "pTreeNode"
    attributes:
      draggable: "true"
    template: _.template """
        <% if (childrenExist) {%>
          <td class="pTreeNodeLabel">
            <div class="pTreeBranch"></div>
            <div class="pTreeLabel"><%= label %></div>
          </td>
          <td>
              <table class="pTreeNodeChildren" cellspacing="0" cellpadding="0">
                  <thead></thead>
                  <tbody></tbody>
              </table>
          </td>
        <% } else { %>
          <td colspan="100%" class="pTreeNodeLabel">
            <div class="pTreeBranch"></div>
            <div class="pTreeLabel pTreeLeafLabel"><%= label %></div>
          </td>
        <% } %>
    """

    serializeData: =>
      label: @model?.get( 'label' ) or ''
      childrenExist: _.size(@collection) > 0

    events:
      'pTree-clone': 'handleReceiveClone'

    ui:
      childTable: '.pTreeNodeChildren'

    initialize: ({@model}) ->
      @collection = @model.get( 'children' )
      @dragMode = 'move'

    appendHtml: (collectionView, itemView) ->
      @ui.childTable.append(itemView.el)

    onRender: ->
      @ui.childTable.attr('border',0)

    onShow: ->
      @$el.hammer({prevent_default: true}).on( 'touch', @handleClick )
      @$el.on( 'dragstart', @handleDragStart )
      @$el.on( 'pTree-move', @handleReceiveMove )


    handleClick: ($event) =>
      $event.stopPropagation()
      @$el.trigger( 'pTree-select', this )

    handleDragStart: ($event) =>
      $event.stopPropagation()
      @startElementPage = @$el.position()

      $('body').addClass('pTree-dragging')
      @$el.addClass( 'pTree-dragging' )
      @$el.css('left',@startElementPage.x).css('top',@startElementPage.y)

      @$el.off( 'dragstart', @handleDragStart )
      @$el.on( 'drag', @handleDragMove )
      @$el.on( 'dragend', @handleDragEnd )

    handleDragMove: ($event) =>
      $event.stopPropagation()
      $dragTargetEl = $(document.elementFromPoint($event.gesture.center.pageX, $event.gesture.center.pageY)).closest('tr')
      @$el.css('left',@startElementPage.x + $event.gesture.deltaX).css('top',@startElementPage.y + $event.gesture.deltaY)
      if $dragTargetEl.is( @$el ) and @dragMode is 'move'
        @$htmlClone.remove() if @$htmlClone?
      else
        @$htmlClone ?= @$el.clone().removeClass( 'pTree-selected' ).addClass( 'pTree-ghost' )
        try
          if not $dragTargetEl.is( @$htmlClone )
            @$lastDragTargetEl = @$dragTargetEl
            @$dragTargetEl = $dragTargetEl
            @$dragTargetEl.after(@$htmlClone) if $event.gesture.deltaY > 0
            @$dragTargetEl.before(@$htmlClone) if $event.gesture.deltaY < 0
        catch
          @$dragTargetEl = @$lastDragTargetEl


    handleDragEnd: ($event) =>
      event.stopPropagation()

      $('body').removeClass('pTree-dragging')
      @$el.removeClass( 'pTree-dragging' )
      @$el.on( 'dragstart', @handleDragStart )
      @$el.off( 'drag', @handleDragEnd )
      @$el.off( 'dragend', @handleDragEnd )

      console.log @$dragTargetEl[0]
      console.log @$el[0]
      @$dragTargetEl.trigger( 'pTree-move', this, $event.gesture.deltaY < 0 ) if @dragMode is 'move'
      @$dragTargetEl.trigger( 'pTree-clone', this, $event.gesture.deltaY < 0 ) if @dragMode is 'clone'

    handleReceiveMove: ($event, view, after) =>
      $event.stopPropagation()
      console.log @model.get( 'label' )
      view.model.reparent( @model.get( 'parent' ), @model.get( 'orderIndex' ) + if after then 0.5 else -0.5 )

    select: ->
      @$el.addClass( 'pTree-selected' )

    deselect: ->
      @$el.removeClass( 'pTree-selected' )

class ascc.pTreeCollectionView extends Backbone.Marionette.CollectionView
    tagName:    "table"
    attributes:
      cellspacing: 0
      cellpadding: 0
      draggable: "true"
    className:  "pTreeCollection"
    itemView:   ascc.pTreeNodeView
    collection: ascc.pTreeCollection

    events:
      'pTree-select': 'handleChangeSelection'

    onRender: =>
      @$el.attr('border',0)

    handleChangeSelection: (event, selection) ->
      @currentSelection.deselect() if @currentSelection?
      @currentSelection = selection
      @currentSelection.select()

    relayout: =>
      _.each @collection.models, (tree) =>
        tree.setRelativeSize();
        tree.setSizeInPixels(@$el.width())
