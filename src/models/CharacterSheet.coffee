Function::property = (prop, desc) ->
  Object.defineProperty @prototype, prop, desc

serenity = window.serenity

class serenity.SerenityUser extends serenity.SerenityBase
  relations:
    [
      type: 'HasMany'
      key: 'characters'
      relatedModel: 'serenity.SerenityCharacter'
      reverseRelation:
        key: 'player'
    ]


class serenity.SerenityBase extends Backbone.RelationalModel
  _stringGetterSetter: ( name, defaultValue = '' ) ->
    @defaults[name] = defaultValue
    ( newValue ) ->
      if newValue?
        @set( name, newValue )
      @get( name )

  _boolGetterSetter: ( name, defaultValue = false ) ->
    @defaults[name] = defaultValue
    ( newValue ) ->
      if newValue?
        @set( name, if newValue then true else false )
      @get( name )

  _integralGetterSetter: ( name, defaultValue = 0 ) ->
    @defaults[name] = defaultValue
    ( newValue ) ->
      newValueInt = parseInt( newValue, 10 )
      if newValue? and not isNaN(newValueInt)
        @set( name, newValueInt )
      else if newValue?
        throw new Error( "Invalid integral value given for #{name}: #{newValue}" )
      @get( name )

  _floatGetterSetter: ( name, defaultValue = 0.0 ) ->
    @defaults[name] = defaultValue
    ( newValue ) ->
      newValueFloat = parseFloat( newValue )
      if newValue? and not isNaN(newValueFloat)
        @set( name, newValueFloat )
      else if newValue?
        throw new Error( "Invalid float value given for #{name}: #{newValue}" )
      @get( name )

  _validatedGetterSetter: ( name, choices, errorStr ) ->
    ( newValue ) ->
      if newValue? and newValue in choices
        @set( name, newValue )
      else if newValue?
        throw new Error(errorStr)
      @get( name )

  _display: ( name, choices ) ->
    ->
      value = @get( name )
      if choices.hasOwnProperty( value )
        choices[value]
      else
        throw new Error("#{name} has invalid type #{value}. Display lookup failed.")

class serenity.SerenityAdventureLog extends serenity.SerenityBase
  defaults:
    date: new Date()
    title: ''
    entry: ''
    author: ''

  title: @_stringGetterSetter( 'title' )
  entry: @_stringGetterSetter( 'entry' )
  author: @_stringGetterSetter( 'author' )

  getInitialText: ->
    @get( 'entry' ).substring(512).replace(/(<([^>]+)>)/ig,"");

class serenity.SerenityItem extends serenity.SerenityBase
  name: @_stringGetterSetter( 'name' )
  type: @_stringGetterSetter( 'type' )
  notes: @_stringGetterSetter( 'notes', '' )

class serenity.SerenityArmor extends serenity.SerenityItem
  rating: @_stringGetterSetter( 'rating' )
  covers: @_stringGetterSetter( 'covers' )
  penalty: @_stringGetterSetter( 'penalty' )

class serenity.SerenityWeapon extends serenity.SerenityItem
  damage: @_stringGetterSetter( 'damage' )
  range: @_stringGetterSetter( 'range' )
  rate: @_stringGetterSetter( 'rate' )
  ammoType: @_stringGetterSetter( 'ammoType' )
  ammo: @_integralGetterSetter( 'ammo', 0 )

class serenity.SerenityCharacterSkillSpecialty extends serenity.SerenityBase
  defaults:
    skill: ''
    specialty: ''
    level: 8

  level: @_validatedGetterSetter( 'level', Object.keys(serenity.DICE_LEVELS).slice(4), 'Invalid level set for skill specialty' )
  levelDisplay: @_display( 'level', serenity.DICE_LEVELS )
  skill: @_validatedGetterSetter( 'skill', Object.keys( serenity.SKILLS ), 'Invalid skill set for skill specialty' )
  skillDisplay: @_display( 'skill', serenity.SKILLS )
  specialty: @_stringGetterSetter( 'specialty' )

class serenity.SerenityAssetComplication extends serenity.SerenityBase
  types: ['Asset', 'Complication']
  defaults:
    name: ''
    description: ''
    type: ''
    level: 2
    requiresSpecialization: false
    original: true
    doubleCost: false
    enabled: true

  name: @_stringGetterSetter( 'name' )
  description: @_stringGetterSetter( 'description' )
  type: @_validatedGetterSetter( 'type', types, 'Invalid type set for asset/complication')
  level: @_validatedGetterSetter( 'level', Object.keys( serenity.ASSET_COMPLICATION_LEVELS ), 'Invalid level set for asset/complication')
  levelDisplay: @_display( 'level', serenity.ASSET_COMPLICATION_LEVELS )
  requiresSpecialization: @_boolGetterSetter( 'requiresSpecialization' )
  original: @_boolGetterSetter( 'original' )
  doubleCost: @_boolGetterSetter( 'doubleCost' )
  enabled: @_boolGetterSetter( 'enabled' )

  shortName: ->
    "#{@name()} (#{if @doubleCost() then 'double ' else ''}#{@levelDisplay()})"

class SerenityCharacter extends serenity.SerenityBase
  _characterSkills: ( names, defaultValue, diceLevelSlice, errorStr ) ->
    _(names).each (name) ->
      @.defaults[name] = defaultValue
      @[name] = _validatedGetterSetter( name, Object.keys( serenity.DICE_LEVELS ).slice(diceLevelSlice), errorStr )

  relations:
    [
      type: 'HasOne'
      key: 'approvedBy'
      relatedModel: 'serenity.SerenityUser'
      reverseRelation:
        key: 'approvedCharacters'
      ,
      type: 'HasMany'
      key: 'weapons'
      relatedModel: 'serenity.SerenityWeapon'
      reverseRelation:
        key: 'character'
      ,
      type: 'HasMany'
      key: 'armor'
      relatedModel: 'serenity.SerenityWeapon'
      reverseRelation:
        key: 'character'
    ]
  # player: reverse relation defined in serenity.SerenityUser

  isNPC: @_boolGetterSetter( 'isNPC' )
  NPCCategory: @_stringGetterSetter( 'npcCategory' )

  name: @_stringGetterSetter( 'name', '' )
  nickname: @_stringGetterSetter( 'nickname', '' )
  homeWorld: @_stringGetterSetter( 'homeWorld', '' )
  concept: @_stringGetterSetter( 'concept', '' )

  plotPoints: @_integralGetterSetter( 'plotPoints', 0 )
  characterGenPoints: @_integralGetterSetter( 'characterGenPoints', 0 )
  unusedAdvancementPoints: @_integralGetterSetter( 'unusedAdvancementPoints', 0 )
  totalAdvancementPoints: @_integralGetterSetter( 'totalAdvancementPoints', 0 )

  credits: @_integralGetterSetter( 'credits', 0 )
  platinum: @_integralGetterSetter( 'platinum', 0 )
  gold: @_integralGetterSetter( 'gold', 0 )
  silver: @_integralGetterSetter( 'silver', 0 )

  stunsTaken: @_integralGetterSetter( 'stunsTaken', 0 )
  woundsTaken: @_integralGetterSetter( 'woundsTaken', 0 )

  equipment: @_stringGetterSetter( 'equipment', '' )
  history: @_stringGetterSetter( 'history', '' )
  description: @_stringGetterSetter( 'description', '' )
  notes: @_stringGetterSetter( 'notes', '' )

  @_characterSkills [
    'strength', 'agility', 'vitality', 'alertness', 'intelligence', 'willpower'
    ], 4, 2,
    "Invalid level set for #{name}"
  @_characterSkills [
    'animalHandling', 'artistry', 'athletics', 'covert', 'discipline', 'craft', 'guns',
    'heavyWeapons', 'influence', 'knowledge', 'mechanicalEngineering', 'linguist',
    'medicalExpertise', 'meleeWeapons', 'perception', 'pilot', 'planetaryVehicles',
    'rangedWeapons', 'scientificExpertise', 'survival', 'technicalEngineering', 'unarmedCombat'
    ], 0, 4,
    "Invalid level set for #{name}"

  totalMoneyInCredits: ->
    @totalMoneyInSilver() * 0.004
  totalMoneyInPlatinum: ->
    @totalMoneyInSilver() * 0.01
  totalMoneyInGold: ->
    @totalMoneyInSilver() * 0.02
  totalMoneyInSilver: ->
    @credits()*250.0 + @platinum()*100.0 + @gold()*50.0 + @silver

  endOfSession: ->
    if @plotPoints() > 6
      @unusedAdvancementPoints( @unusedAdvancementPoints() + @plotPoints() - 6 )
    @plotPoints( 6 )

  lifePoints: ->
    @vitality() + @willpower()

  endurance: ->
    @vitality() + @willpower()

  initiative: ->
    @agility() + @alertness()

  resistance: ->
    2*@vitality()

  passedOut: ->
    @stunsTaken() + @woundsTaken() >= 18

  dead: ->
    @woundsTaken() >= @lifePoints()

  knockedSenseless: ->
    @stunsTaken() >= @lifePoints()

  alive: ->
    not @dead()

  conscious: ->
    not (@knockedSenseless() or @passedOut() or @dead())

  approved: ->
    @approved_by?

class SerenityCharacterAsset extends serenity.SerenityBase
  #character and asset must be unique together
  relations:
    [
      type: 'HasOne'
      key: 'character'
      relatedModel: 'serenity.SerenityCharacter'
      reverseRelation:
        key: 'assets'
    ,
      type: 'HasOne'
      key: 'asset'
      relatedModel: 'serenity.SerenityAssetComplication'
    ]
  specialization: @_stringGetterSetter( 'specialization' )

  assetName: ->
    @asset.shortName()
  asset_effect: ->
    @asset.description()

class SerenityCharacterComplication extends serenity.SerenityBase
  #character and complication must be unique together
  relations:
    [
      type: 'HasOne'
      key: 'character'
      relatedModel: 'serenity.SerenityCharacter'
      reverseRelation:
        key: 'complications'
    ,
      type: 'HasOne'
      key: 'complication'
      relatedModel: 'serenity.SerenityAssetComplication'
    ]
  specialization: @_stringGetterSetter( 'specialization' )

  assetName: ->
    @complication.shortName()
  asset_effect: ->
    @complication.description()

class SerenityDiceRoller extends serentiy.SerenityBase
  relations:
    [
      type: 'HasOne'
      key: 'character'
      relatedModel: 'serenity.SerenityCharacter'
      reverseRelation:
        key: 'rolls'
    ]
  defaults:
    created: new Date().toLocaleString()

  description: @_stringGetterSetter( 'description' )
  d2: @_integralGetterSetter( 'd2' )
  d4: @_integralGetterSetter( 'd4' )
  d6: @_integralGetterSetter( 'd6' )
  d8: @_integralGetterSetter( 'd8' )
  d10: @_integralGetterSetter( 'd10' )
  d12: @_integralGetterSetter( 'd12' )

  result: @_integralGetterSetter( 'result' )

  _randint: ( min, max ) -> Math.floor(Math.random() * (max - min + 1)) + min

  roll: ->
    result = @result()
    if @result() is 0
      _([0..@d2()]).each -> result += @_randint(1,2)
      _([0..@d4()]).each -> result += @_randint(1,4)
      _([0..@d6()]).each -> result += @_randint(1,6)
      _([0..@d8()]).each -> result += @_randint(1,8)
      _([0..@d10()]).each -> result += @_randint(1,10)
      _([0..@d12()]).each -> result += @_randint(1,12)
      @result( result )
    result
