serenity = window.serenity

serenity.DICE_LEVELS =
  0: '0'
  2: 'd2'
  4: 'd4'
  6: 'd6'
  8: 'd8'
  10: 'd10'
  12: 'd12'
  14: 'd12+2'
  16: 'd12+4'


serenity.SKILLS =
  animal_handling:'Animal Handling'
  artistry: 'Artistry'
  athletics: 'Athletics'
  covert: 'Covert'
  craft: 'Craft'
  discipline: 'Discipline'
  guns: 'Guns'
  heavy_weapons: 'Heavy Weapons'
  influence: 'Influence'
  knowledge: 'Knowledge'
  mechanical_engineering: 'Mechanical Engineering'
  linguist: 'Linguist'

  medical_expertise: 'Medical Expertise'
  melee_weapons: 'Melee Weapons'
  perception: 'Perception'
  performance: 'Performance'
  pilot: 'Pilot'
  planetary_vehicles: 'Planetary Vehicles'
  ranged_weapons: 'Ranged Weapons'
  scientific_expertise: 'Scientific Expertise'
  survival: 'Survival'
  technical_engineering: 'Technical Engineering'
  unarmed_combat: 'Unarmed Combat'


serenity.ASSET_COMPLICATION_LEVELS =
  2:'Minor'
  4:'Major'


#def generate_assets():
#from assets import original_assets, original_assets_with_specialization
#from assets import new_assets, new_assets_double_cost, new_assets_with_specialization
#def create_assets(asset_tuples, **kwargs):
#from models import SerenityAssetComplication
#AllAssets = SerenityAssetComplication.objects.filter(type="asset", **kwargs)
#for name, description, level in asset_tuples:
#if not AllAssets.all().filter(name=name,level=level):
#NewAsset = SerenityAssetComplication(name=name, description=description, level=level, type="asset", **kwargs)
#NewAsset.save()
#create_assets(original_assets, original=True, requires_specialization=False)
#create_assets(original_assets_with_specialization, original=True, requires_specialization=True)
#create_assets(new_assets, original=False, requires_specialization=False)
#create_assets(new_assets_double_cost, original=False, requires_specialization=False, double_cost=True)
#create_assets(new_assets_with_specialization, original=False, requires_specialization=True)
#
#def generate_complications():
#from complications import original_complications, original_complications_with_specialization
#from complications import new_complications, new_complications_with_specialization
#def create_complications(complication_tuples, **kwargs):
#from models import SerenityAssetComplication
#AllComplications = SerenityAssetComplication.objects.filter(type="complication",**kwargs)
#for name, description, level in complication_tuples:
#if not AllComplications.all().filter(name=name,level=level):
#NewComplication = SerenityAssetComplication(name=name, description=description, level=level, type="complication", **kwargs)
#NewComplication.save()
#
#create_complications(original_complications, original=True, requires_specialization=False)
#create_complications(original_complications_with_specialization, original=True, requires_specialization=True)
#create_complications(new_complications, original=False, requires_specialization=False)
#create_complications(new_complications_with_specialization, original=False, requires_specialization=True)