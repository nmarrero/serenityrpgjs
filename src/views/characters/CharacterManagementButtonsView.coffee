
serenity = window.serenity

class serenity.characters.CharacterManagementButtonsView extends Backbone.Marionette.View
  className: "serenity-character-management-buttons"
  template: _.template """
      <div class="serenity-character-management-add-npc-button"></div>
      <div class="serenity-character-management-add-pc-button"></div>
      <div class="serenity-character-management-delete-button"></div>
      <div class="serenity-character-management-duplicate-button"></div>
      <div class="serenity-character-management-see-changes-button"></div>
  """

  regions:
    'addNPCButton': 'serenity-character-management-add-npc-button'
    'addPCButton': 'serenity-character-management-add-pc-button'
    'deleteButton': 'serenity-character-management-delete-button'
    'duplicateButton': 'serenity-character-management-duplicate-button'
    'differencesButton': 'serenity-character-management-see-changes-button'

  onShow: ->
    @regions.addNPCButton.show( new serenity.characters.AddNPCButtonView() )
    @regions.addPCButton.show( new serenity.characters.AddPCButtonView() )
    @regions.deleteButton.show( new serenity.characters.DeleteCharacterButtonView() )
    @regions.duplicateButton.show( new serenity.characters.DuplicateCharacterButtonView() )
    @regions.differencesButton.show( new serenity.characters.ShowCharacterDifferencesButtonView() )


